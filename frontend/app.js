const HOST = "http://localhost:8080";



const voteRegister = () => {
  // getting the value from the from
  const userName = document.querySelector("#username").value;
  const userAge = document.querySelector("#age").value;
  
  var ele = document.getElementsByName('fav_activity');
  var selectedValue;           
  for(i = 0; i < ele.length; i++) {
      if(ele[i].checked)
      selectedValue=  ele[i].value;
  }
  
  const options = selectedValue;

  $.ajax({
    method: "post",
    url: `${HOST}/users/signup`,
    data: JSON.stringify({
      "username": userName,
      "age": userAge,
      "options": options,
    }),
    headers: {
     'Accept': "application/json",
      'Content-type': "application/json",
    },
  })
    .done((response) => {

      const sendData= document.getElementById("sendData");
      sendData.classList.toggle("hidden");
      sendData.innerHTML= `Voted successful`;
      const hiddeForm= document.querySelector("#voteForm");
      hiddeForm.style.display="none";
      const reload = document.getElementById("reload-enterUser");
      const enterUser = document.getElementById("enterUser");
      enterUser.classList.toggle("hidden");
      reload.classList.toggle("hidden");
    }
    )

    .fail((obj, textStatus) => {
      if ((obj && obj.responseText)) {
       // const messageDiv = document.querySelector("#message");
        // $(messageDiv).to
        //messageDiv.classList.toggle('d-none');
       // alert(obj.responseText);
        const sendData= document.getElementById("sendData");
      sendData.classList.toggle("hidden");
      sendData.innerHTML= `${obj.responseText}`;
      const hiddeForm= document.querySelector("#voteForm");
      hiddeForm.style.display="none";
      const reload = document.getElementById("reload-enterUser");
      const enterUser = document.getElementById("enterUser");
      enterUser.classList.toggle("hidden");
      reload.classList.toggle("hidden");
      }
    });
};


const checkVote = () => {
  // getting the value from the from
  const username = document.querySelector("#checkUserVote").value;

  // $.get(HOST, function(data, status){
  //   alert("Data: " + data + "\nStatus: " + status);
  // });

  fetch(`${HOST}/users/${username}`)
  .then(data=>data.json())
  .then((json)=>{
    alert(JSON.stringify(json))
  })

  // $.ajax({
  //   method: "get",
  //   url: `${HOST}/users/${username}`
  // }).done((response) => {
  //     for(const obj of response){
  //         alert(obj);
  //        // document.querySelector("#loadData").innerHTML += '<label>' + obj.options + '</label>'
  //     }
  //   }).fail((obj, textStatus) => {
  //     if ((obj && obj.responseText)) {
  //       alert(obj.responseText.message);
  //     }
  //   });
};

async function getData(){
  const username = document.querySelector("#checkUserVote").value;
  const result = await fetch(`${HOST}/users/${username}`);
  result.json().then(json =>{

    var age= json.age;
    var options= json.options;
  //  alert(`${age}, ${options}`);

    const loadData= document.getElementById("loadData");
    const reload = document.getElementById("reload-checkUser");
    const checkUser = document.getElementById("checkUser");
    const hiddeForm= document.querySelector("#checkVoteForm");
    // hiddeForm.setAttribute("disabled", true);
    hiddeForm.style.display="none";
    checkUser.classList.toggle("hidden");
    reload.classList.toggle("hidden");
    loadData.classList.toggle("hidden");
    loadData.innerHTML= `The User age is ${age} and the activity liked is ${options}`;
    
  }).catch((ErrorEvent)=>{
    //alert((ErrorEvent))
    const loadData= document.getElementById("loadData");
    const reload = document.getElementById("reload-checkUser");
    const checkUser = document.getElementById("checkUser");
    const hiddeForm= document.querySelector("#checkVoteForm");
    // hiddeForm.setAttribute("disabled", true);
    hiddeForm.style.display="none";
    checkUser.classList.toggle("hidden");
    reload.classList.toggle("hidden");
    loadData.classList.toggle("hidden");
    loadData.innerHTML= ` The user doesnot exist <br> ${ErrorEvent}`;

  })
}

async function delData(){
  const delUserName= document.querySelector("#user-delete").value;

  const deleteData= document.getElementById("deleteData");
  const reload = document.getElementById("reload-deletePoll");
  const deleteUserResult = document.getElementById("deletePoll");
  const hiddeForm= document.querySelector("#deleteUserForm");

  fetch(`${HOST}/users/${delUserName}`, { method: 'DELETE' })
    .then(async response => {
        const isJson = response.headers.get('content-type')?.includes('application/json');
        const data = isJson && await response.json();

        // check for error response
        if (!response.ok) {
            // get error message from body or default to response status
            const error = (data && data.message) || response.status;
            return Promise.reject(error);
        }

       
          hiddeForm.style.display="none";
          deleteUserResult.classList.toggle("hidden");
          reload.classList.toggle("hidden");
          deleteData.classList.toggle("hidden");
          deleteData.innerHTML= `The poll result have been deleted`;
    })
    .catch(Error => {
        // element.parentElement.innerHTML = `Error: ${error}`;
        // console.error('There was an error!', error);
        hiddeForm.style.display="none";
        deleteUserResult.classList.toggle("hidden");
        reload.classList.toggle("hidden");
        deleteData.classList.toggle("hidden");
        deleteData.innerHTML= `The user doesnot exist Error: ${Error}`;


    });


  // const delResult = await fetch(`${HOST}/users/${delUserName}`, {
  //           method: 'DELETE',
  //           headers: {
  //               'Content-type': 'application/json'
  //           }
  // });
  // delResult.json().then((data)=>{
  //   const deleteData= document.getElementById("deleteData");
  //   const reload = document.getElementById("reload-deletePoll");
  //   const deleteUserResult = document.getElementById("deletePoll");
  //   const hiddeForm= document.querySelector("#deleteUserForm");
  //   hiddeForm.style.display="none";
  //   deleteUserResult.classList.toggle("hidden");
  //   reload.classList.toggle("hidden");
  //   deleteData.classList.toggle("hidden");
  //   deleteData.innerHTML= `The poll result have been deleted`;
  //  })
  // .catch(error=>{
  //   alert(error)})
  
}



const enterUser = document.getElementById("enterUser");
enterUser.addEventListener("click", voteRegister);

const checkUser = document.getElementById("checkUser");
checkUser.addEventListener("click", getData);

const deletePoll = document.getElementById("deletePoll");
deletePoll.addEventListener("click",delData);