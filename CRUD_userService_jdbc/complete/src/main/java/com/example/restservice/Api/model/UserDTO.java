package com.example.restservice.Api.model;

import lombok.Data;

@Data
public class UserDTO {
    private int id;
    private String username;
    private int age;
    private String options;
}
