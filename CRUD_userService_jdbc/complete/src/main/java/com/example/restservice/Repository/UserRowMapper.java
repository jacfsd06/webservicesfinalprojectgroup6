package com.example.restservice.Repository;

import com.example.restservice.Model.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        return User.builder().id(rs.getInt("id")).
                username(rs.getString("username")).
                age(rs.getInt("age")).
                options(rs.getString("options")).
                build();
    }
}