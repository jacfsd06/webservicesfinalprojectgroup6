package com.example.restservice.Api.model;

import com.example.restservice.Model.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * @author Shuwen Ju
 */
@Component
public class UserConverter {
    @Autowired
    private ModelMapper modelMapper;

    public UserDTO converToUserDTO(User user){
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        return userDTO;
    }
}
