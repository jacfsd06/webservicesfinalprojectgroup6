package com.example.restservice.Model;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResultSet {
    private int totalCount;
    private int snowBoardingCount;
    private int sleddingCount;
    private int iceSkatingCount;
    private int skiingCount;
    private int otherCount;
}
