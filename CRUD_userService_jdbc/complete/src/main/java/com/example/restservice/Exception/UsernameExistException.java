package com.example.restservice.Exception;


/**
 * @author Shuwen Ju
 */
public class UsernameExistException extends RuntimeException{
    public UsernameExistException(String message){
        super(message);
    }
}