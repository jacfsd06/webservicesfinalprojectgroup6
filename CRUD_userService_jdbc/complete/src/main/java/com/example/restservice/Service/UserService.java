package com.example.restservice.Service;

import com.example.restservice.Exception.UsernameExistException;
import com.example.restservice.Exception.UserNotFoundException;
import com.example.restservice.Model.ResultSet;
import com.example.restservice.Model.User;
import com.example.restservice.Repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.example.restservice.Constants.ErrorMessage.ERROR_USER_EXISTS;
import static com.example.restservice.Constants.ErrorMessage.ERROR_USER_NOT_FOUND;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User signUp(User user) throws UsernameExistException {
//        List<User> allUser = userRepository.userList();
//        if(allUser.size() > 0) {
//        for (int i = 0; i < allUser.size(); i++) {
//            if (user.getUsername().equals(allUser.get(i).getUsername())) {
//                throw new UsernameExistException(ERROR_USER_EXISTS);
//        }
//    }
//        }
        Optional<User> foundUser = userRepository.getUser(user.getUsername());
        if (foundUser.isPresent()) {
            throw new UsernameExistException(ERROR_USER_EXISTS);
    }
        return userRepository.signUp(user);
    }

    public Optional<User> getUser(String username) {
        Optional<User> user = userRepository.getUser(username);
        if (user.isPresent()) {
            return user;
        } else {
            throw new UserNotFoundException(ERROR_USER_NOT_FOUND);
        }
    }

    public User updateOption(String username, String option) {
        Optional<User> foundUser = userRepository.getUser(username);
        if (!foundUser.isPresent()) {
            throw new UserNotFoundException(ERROR_USER_NOT_FOUND);
        } else {
            return userRepository.updateOption(username, option);
        }
    }

    public void deleteUser(String username) {
        Optional<User> foundUser = userRepository.getUser(username);
        if (!foundUser.isPresent()) {
            throw new UserNotFoundException(ERROR_USER_NOT_FOUND);
        } else {
            userRepository.deleteUser(username);
        }
    }

    public ResultSet resultData() {
        return  userRepository.resultData();

    }
}
