package com.example.restservice.Constants;

/**
 * @author Shuwen Ju
 */
public class ErrorMessage {
    public static final String ERROR_USER_NOT_FOUND = "User not found.";
    public static final String ERROR_USER_EXISTS = "Seems like you already voted.";


}