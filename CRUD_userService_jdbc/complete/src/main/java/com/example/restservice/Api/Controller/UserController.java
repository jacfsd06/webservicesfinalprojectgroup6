package com.example.restservice.Api.Controller;


import com.example.restservice.Api.model.UserConverter;
import com.example.restservice.Api.model.UserDTO;
import com.example.restservice.Exception.UsernameExistException;
import com.example.restservice.Exception.UserNotFoundException;
import com.example.restservice.Model.ResultSet;
import com.example.restservice.Model.User;
import com.example.restservice.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin(maxAge = 45000)
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    private UserConverter userConverter;

    @PostMapping("/signup")
    public ResponseEntity<UserDTO> signUp(@RequestBody User user) {
        try{
            User newUser = userService.signUp(user);
            return new ResponseEntity<>(userConverter.converToUserDTO(newUser), HttpStatus.CREATED);
        }catch (UsernameExistException exception){
            return new ResponseEntity(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{username}")
    public ResponseEntity<UserDTO> getUser(@PathVariable String username) {
        try {
            User foundUser = userService.getUser(username).get();
            return new ResponseEntity<>(userConverter.converToUserDTO(foundUser),HttpStatus.OK);
        }catch (UserNotFoundException exception){
            return new ResponseEntity(exception.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{username}")
    public ResponseEntity<UserDTO> updateUser(@PathVariable String username, @RequestBody User user) {
        try {
            User updatedUser = userService.updateOption(username, user.getOptions());
            return ResponseEntity.ok(userConverter.converToUserDTO(updatedUser));
        }catch (UserNotFoundException exception){
            return new ResponseEntity(exception.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{username}")
    public ResponseEntity<Void> deleteUser(@PathVariable String username) {
        try {
            userService.deleteUser(username);
            return ResponseEntity.noContent().build();
        }catch (UserNotFoundException exception){
            return new ResponseEntity(exception.getMessage(), HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/all")
    public ResponseEntity<ResultSet>  resultData(){
        return new ResponseEntity<>(userService.resultData(),HttpStatus.OK);
    }
}
