package com.example.restservice.Exception;
/**
 * @author Shuwen Ju
 */
public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(String message){
        super(message);
    }
}