package com.example.restservice.Repository;

import com.example.restservice.Exception.UserNotFoundException;
import com.example.restservice.Exception.UsernameExistException;
import com.example.restservice.Model.ResultSet;
import com.example.restservice.Model.User;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {
    private final JdbcTemplate jdbcTemplate;

    public UserRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public List<User> userList(){
        return jdbcTemplate.query("SELECT * FROM users", new UserRowMapper());
    }

    public User signUp(User user) throws UsernameExistException {
        jdbcTemplate.update("INSERT INTO users (username, age, `options`) VALUES (?,?,?)",
                user.getUsername(), user.getAge(), user.getOptions());
        int id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        user = User.builder().id(id).username(user.getUsername()).age(user.getAge())
                .options(user.getOptions()).build();
        return user;
    }

    public Optional<User> getUser(String username) {
        try {
            String sql = "SELECT * FROM users WHERE username=?";
//            User user = jdbcTemplate.queryForObject(sql, new Object[]{id},
//                    (rs, rowNum) -> User.builder().id(rs.getInt("id")).username(rs.getString("username"))
//                            .age(rs.getInt("age")).build());

            User user = jdbcTemplate.queryForObject(sql,new UserRowMapper(), username);

            return Optional.of(user);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public User updateOption(String username, String options) throws UserNotFoundException {
        User user = getUser(username).get();
        jdbcTemplate.update("UPDATE users SET options=? WHERE username=?",options, username);
        return User.builder().id(user.getId()).username(username).age(user.getAge()).options(options).build();
    }

    public void deleteUser(String username) {
        jdbcTemplate.update("DELETE FROM users WHERE username=?", username);
    }

    public ResultSet resultData(){
        String sql = "SELECT Count(*) FROM  users";
        Integer  total=   jdbcTemplate.queryForObject(sql, Integer.class);
        String sql1=  "SELECT Count(*) FROM users WHERE `options`= 'snowboarding' ";
        Integer  snowboarding=   jdbcTemplate.queryForObject(sql1, Integer.class);
        String sql2= "SELECT Count(*) FROM users WHERE `options`= 'sledding '";
        Integer  sledding=   jdbcTemplate.queryForObject(sql2, Integer.class);
        String sql3= "SELECT Count(*) FROM users WHERE `options`= 'Ice skating '";
        Integer  iceSkating=   jdbcTemplate.queryForObject(sql3, Integer.class);
        String sql4= "SELECT Count(*) FROM users WHERE `options`= 'Skiing ' ";
        Integer  skiing=   jdbcTemplate.queryForObject(sql4, Integer.class);
        String sql5= "SELECT Count(*) FROM users WHERE `options`= 'other' ";
        Integer  other=   jdbcTemplate.queryForObject(sql5, Integer.class);

        return  ResultSet.builder().totalCount(total.intValue())
                .snowBoardingCount(snowboarding.intValue())
                .sleddingCount(sledding.intValue())
                .iceSkatingCount(iceSkating.intValue())
                .skiingCount(skiing.intValue())
                .otherCount(other.intValue()).build();




    }

}