package com.example.restservice.Configuration;

import org.springframework.context.annotation.Bean;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Configuration;

/**
 * @author Shuwen Ju
 */
@Configuration
public class BeanConfiguration {
    //    @Bean
//    public RestTemplate restTemplate(){
//        return new RestTemplate();
//    }
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
}
