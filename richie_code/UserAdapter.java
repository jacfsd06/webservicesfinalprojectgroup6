package com.example.restservice.group6project;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UserAdapter {
    private final JdbcTemplate jdbcTemplate;

    public UserAdapter(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public User signUp(User user) {
        jdbcTemplate.update("INSERT INTO users (username, age) VALUES (?,?)", user.getUsername(), user.getAge());
        int id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        return User.builder().id(id).username(user.getUsername()).age(user.getAge()).build();
    }

    public Optional<User> getUser(int id) {
        try {
            User user = jdbcTemplate.queryForObject("SELECT * FROM users WHERE id=?", new Object[]{id},
                    (rs, rowNum) -> User.builder().id(rs.getInt("id")).username(rs.getString("username"))
                            .age(rs.getInt("age")).build());
            return Optional.of(user);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public User updateUser(int id, User user) {
        jdbcTemplate.update("UPDATE users SET username=?, age=? WHERE id=?", user.getUsername(), user.getAge(), id);
        return User.builder().id(id).username(user.getUsername()).age(user.getAge()).build();
    }

    public void deleteUser(int id) {
        jdbcTemplate.update("DELETE FROM users WHERE id=?", id);
    }
}