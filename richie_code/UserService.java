package com.example.restservice.group6project;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    private final UserAdapter userAdapter;

    public UserService(UserAdapter userAdapter) {
        this.userAdapter = userAdapter;
    }

    public User signUp(User user) {
        return userAdapter.signUp(user);
    }

    public Optional<User> getUser(int id) {
        return userAdapter.getUser(id);
    }

    public User updateUser(int id, User user) {
        return userAdapter.updateUser(id, user);
    }

    public void deleteUser(int id) {
        userAdapter.deleteUser(id);
    }
}